# license-service

#### 介绍
License服务端，用于生成license证书

#### 软件架构
springboot + swagger2

#### 使用说明

1. 修改 application.yml 配置文件中的 licensePath 为自己本地路径
2. 启动 Application
3. 访问项目地址：http://127.0.0.1:8090/api/doc.html

#### 项目功能
1.获取服务器硬件信息，osName参数可传入windows或linux
![img.png](img.png)

2.根据获取到的硬件信息进行加密生成License证书
![img_1.png](img_1.png)


#### 调试步骤
1 生成privateKeys.store和publicCerts.store  
  1.1 首先要用KeyTool工具来生成密钥库：（-alias别名 –validity 3650表示10年有效）   
      keytool -genkey -alias privatekeys -keysize 1024 -keystore privateKeys.store -validity 3650  
  1.2 然后将密钥库中名称为‘privatekeys’的证书条目导出到证书文件certfile.cer中：  
      keytool -export -alias privatekeys -file certfile.cer -keystore privateKeys.store  
  1.3 然后再把这个证书文件的信息导入到公钥库中别名为publiccert的证书条目中：   
      keytool -import -alias publiccert -file certfile.cer -keystore publicCerts.store  
  1.4 最后生成的文件privateKeys.store和publicCerts.store拷贝出来备用。  
  默认将 privateKeys.store 放到license-service根目录下，将publicCerts.store放到license-service\application\src\main\resources下  
  
2 编译并启动 Application  
  2.1 mvn clean package  
      java -jar .\application\target\application-1.0.0.jar   
  2.2 访问项目地址：http://127.0.0.1:8090/api/doc.html  
  2.3 生成证书  
      参考参数  
     {  
				"consumerAmount": 1,  
				"consumerType": "user",  
				"description": "1",  
				"expiryTime": "2024-1-1 18:00:00",  
				"issuedTime": "2023-9-6 17:00:00",  
				"keyPass": "123456a",  
				"licenseCheckModel": {  
			  "ipAddress": [  ],  
			  "macAddress": [   ],  
			  "mainBoardSerial": ""  
				},  
				"licensePath": "./License/license.lic",  
				"privateAlias": "privatekeys",  
				"privateKeysStorePath": "./license/privateKeys.store",  
				"storePass": "123456a",  
				"subject": "jiuxin"  
		 }  
	2.3 验证证书  
	  	参考参数  
	  	{  
				"licensePath": "./License/license.lic",  
				"publicAlias": "publiccert",  
				"publicKeysStorePath": "/publicCerts.store",  
				"storePass": "123456a",  
				"subject": "jiuxin"  
			}   
  			
		 