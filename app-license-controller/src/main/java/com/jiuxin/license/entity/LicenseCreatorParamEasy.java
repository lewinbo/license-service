package com.jiuxin.license.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: jiuxin
 * @ClassName LicenseCreatorParamEasy
 * @Description TODO
 * @date 2022/12/9 11:02
 * @Version 1.0
 */
@ApiModel("简易生成证书实体类")
public class LicenseCreatorParamEasy implements Serializable {

    private static final long serialVersionUID = 2832129012982731725L;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("证书生效时间")
    private Date issuedTime = new Date();

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("证书失效时间")
    private Date expiryTime;

    @ApiModelProperty("额外的服务器硬件校验信息")
    private LicenseCheckModel licenseCheckModel;

    public Date getIssuedTime() {
        return issuedTime;
    }

    public void setIssuedTime(Date issuedTime) {
        this.issuedTime = issuedTime;
    }

    public Date getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Date expiryTime) {
        this.expiryTime = expiryTime;
    }

    public LicenseCheckModel getLicenseCheckModel() {
        return licenseCheckModel;
    }

    public void setLicenseCheckModel(LicenseCheckModel licenseCheckModel) {
        this.licenseCheckModel = licenseCheckModel;
    }
}