package com.jiuxin.license.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>License校验类需要的参数</p>
 *
 * @author jiuxin
 * @version V.0.2.1
 */
@ApiModel("简易验证证书实体类")
public class LicenseVerifyParameEasy {

    /**证书路径*/
    private String licensePath;

    /**特殊Key*/
    private String specialKey;


    public LicenseVerifyParameEasy() {

    }

    public LicenseVerifyParameEasy(String licensePath, String specialKey) {

        this.licensePath = licensePath;
        this.specialKey = specialKey;
    }

    public String getLicensePath() {
        return licensePath;
    }

    public void setLicensePath(String licensePath) {
        this.licensePath = licensePath;
    }

    public String getSpecialKey() {
        return specialKey;
    }

    public void setSpecialKey(String specialKey) {
        this.specialKey = specialKey;
    }


}
