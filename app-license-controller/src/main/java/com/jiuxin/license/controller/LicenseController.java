package com.jiuxin.license.controller;

import com.jiuxin.license.conf.AbstractServerInfos;
import com.jiuxin.license.conf.LinuxServerInfos;
import com.jiuxin.license.conf.WindowsServerInfos;
import com.jiuxin.license.entity.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: jiuxin
 * @ClassName LicenseController
 * @Description TODO
 * @date 2022/12/9 11:15
 * @Version 1.0
 */
@RestController
@Api(tags = "license证书")
@RequestMapping("/license")
public class LicenseController {

    /**
     * 证书生成路径
     */
    @Value("${license.licensePath}")
    private String licensePath;

    /**
    * @Author: jiuxin
    * @Description: 获取服务器硬件信息
    * @DateTime: 11:18 2022/12/9
    * @Params:  osName 系统类型 windows或linux
    * @Return
    */
    @ApiOperation("获取服务器硬件信息")
    @GetMapping("/getServerInfos/{osName}")
    public LicenseCheckModel getServerInfos(@PathVariable String osName) {
        //操作系统类型
        if (StringUtils.isBlank(osName)) {
            osName = System.getProperty("os.name");
        }
        osName = osName.toLowerCase();

        AbstractServerInfos abstractServerInfos = null;

        //根据不同操作系统类型选择不同的数据获取方法
        if (osName.startsWith("windows")) {
            abstractServerInfos = new WindowsServerInfos();
        } else if (osName.startsWith("linux")) {
            abstractServerInfos = new LinuxServerInfos();
        } else {//其他服务器类型
            abstractServerInfos = new LinuxServerInfos();
        }

        return abstractServerInfos.getServerInfos();
    }

    /**
    * @Author: jiuxin
    * @Description: 生成证书
    * @DateTime: 11:18 2022/12/9
    * @Params:
    * @Return
    */
    @ApiOperation("生成证书")
    @PostMapping("/generateLicense")
    public Map<String, Object> generateLicense(@RequestBody LicenseCreatorParam param) {
        Map<String, Object> resultMap = new HashMap<>(2);

        if (StringUtils.isBlank(param.getLicensePath())) {
            param.setLicensePath(licensePath);
        }

        LicenseCreator licenseCreator = new LicenseCreator(param);
        boolean result = licenseCreator.generateLicense();

        if (result) {
            resultMap.put("result", "ok");
            resultMap.put("msg", param);
        } else {
            resultMap.put("result", "error");
            resultMap.put("msg", "证书文件生成失败！");
        }

        return resultMap;
    }

    /**
     * @Author: jiuxin
     * @Description: 生成证书
     * @DateTime: 11:18 2022/12/9
     * @Params:
     * @Return
     */
    @ApiOperation("简易生成证书")
    @PostMapping("/generateLicenseEasy")
    public Map<String, Object> generateLicenseEasy(@RequestBody LicenseCreatorParamEasy paramE) {
        Map<String, Object> resultMap = new HashMap<>(2);

        LicenseCreatorParam param = new LicenseCreatorParam();
        param.setIssuedTime(paramE.getIssuedTime());
        param.setExpiryTime(paramE.getExpiryTime());
        param.setLicenseCheckModel(paramE.getLicenseCheckModel());


        LicenseCreator licenseCreator = new LicenseCreator(param);
        boolean result = licenseCreator.generateLicense();

        if (result) {
            resultMap.put("result", "ok");
            resultMap.put("msg", param);
        } else {
            resultMap.put("result", "error");
            resultMap.put("msg", "证书文件生成失败！");
        }

        return resultMap;
    }

    /**
     * @Author: jiuxin
     * @Description: 验证证书
     * @DateTime: 11:18 2022/12/9
     * @Params:
     * @Return
     */
    @ApiOperation("验证证书")
    @PostMapping("/verifyLicense")
    public Map<String, Object> verifyLicense(@RequestBody LicenseVerifyParam param) {
        Map<String, Object> resultMap = new HashMap<>(2);

        LicenseVerifier licenseVerifier = new LicenseVerifier();

        boolean result = true;
        result = licenseVerifier.install(param);

        if (result) {
            resultMap.put("result", "ok");
            resultMap.put("msg", param);
        } else {
            resultMap.put("result", "error");
            resultMap.put("msg", "证书文件安装失败！");
            return resultMap;
        }

        result = licenseVerifier.verify(param);

        if (result) {
            resultMap.put("result", "ok");
            resultMap.put("msg", param);
        } else {
            resultMap.put("result", "error");
            resultMap.put("msg", "证书文件验证失败！");
        }

        return resultMap;
    }

    /**
     * @Author: jiuxin
     * @Description: 简易验证证书
     * @DateTime: 11:18 2022/12/9
     * @Params:
     * @Return
     */
    @ApiOperation("简易验证证书")
    @PostMapping("/verifyLicenseEasy")
    public Map<String, Object> verifyLicenseEasy(@RequestBody LicenseVerifyParameEasy paramE) {
        Map<String, Object> resultMap = new HashMap<>(2);

        LicenseVerifier licenseVerifier = new LicenseVerifier();

        boolean result = true;

        LicenseVerifyParam param = new LicenseVerifyParam();
        param.setLicensePath(paramE.getLicensePath());
        param.setSpecialKey(paramE.getSpecialKey());
        result = licenseVerifier.install(param);

        if (result) {
            resultMap.put("result", "ok");
            resultMap.put("msg", param);
        } else {
            resultMap.put("result", "error");
            resultMap.put("msg", "证书文件安装失败！");
            return resultMap;
        }

        result = licenseVerifier.verify(param);

        if (result) {
            resultMap.put("result", "ok");
            resultMap.put("msg", param);
        } else {
            resultMap.put("result", "error");
            resultMap.put("msg", "证书文件验证失败！");
        }

        return resultMap;
    }




}