package com.jiuxin.license.controller;

import com.jiuxin.license.entity.CustomLicenseManager;
import com.jiuxin.license.entity.LicenseVerifyParam;
import de.schlichtherle.license.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.prefs.Preferences;

/**
 * <p>License校验类</p>
 *
 * @author appleyk
 * @version V.0.2.1
 * @blob https://blog.csdn.net/appleyk
 * @date created on  10:42 下午 2020/8/21
 */
public class LicenseVerifier {

    private static Logger logger = LogManager.getLogger(LicenseVerifier.class);

    /**
     * <p>安装License证书</p>
     * @param param License校验类需要的参数
     */
    public synchronized boolean install(LicenseVerifyParam param){
        Boolean rtn = true;
        try{
            /** 1、初始化License证书参数 */
            LicenseParam licenseParam = initLicenseParam(param);
            /** 2、创建License证书管理器对象 */
//          LicenseManager licenseManager =new CustomLicenseManager(licenseParam);
            //走自定义的Lic管理
            CustomLicenseManager licenseManager = new CustomLicenseManager(licenseParam, param.getSpecialKey());
            /** 3、获取要安装的证书文件 */
            File licenseFile = ResourceUtils.getFile(param.getLicensePath());
            /** 4、如果之前安装过证书，先卸载之前的证书 == 给null */
            licenseManager.uninstall();
            /** 5、开始安装 */
            LicenseContent content = licenseManager.install(licenseFile);

        }catch (LicenseContentException contentExc){
            String message = contentExc.getMessage();
            logger.error(message);
            rtn =  false;

        } catch (Exception e){
            logger.error(e.getMessage(),e);
            rtn =  false;
        }

        return  rtn;
    }

    /**
     * <p>校验License证书</p>
     * @param param License校验类需要的参数
     */
    public Boolean verify(LicenseVerifyParam param){
        Boolean rtn = true;

        /** 1、初始化License证书参数 */
        LicenseParam licenseParam = initLicenseParam(param);
        /** 2、创建License证书管理器对象 */
        LicenseManager licenseManager = new CustomLicenseManager(licenseParam, param.getSpecialKey());
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        /** 3、开始校验证书 */
        try {
            LicenseContent licenseContent = licenseManager.verify();
            String message = MessageFormat.format("证书校验通过，证书有效期：{0} - {1}",
                    format.format(licenseContent.getNotBefore()),format.format(licenseContent.getNotAfter()));
            logger.info(message);

        }catch (NoLicenseInstalledException ex){
            String message = "证书未安装！";
            logger.error(message,ex);
            rtn = false;

        }catch (LicenseContentException cex){
            logger.error(cex.getMessage(),cex);
            rtn = false;
        } catch (Exception e){
            String message = "证书校验失败！";
            logger.error(message,e);
            rtn = false;
        }

        return   rtn;
    }

    /**
     * <p>初始化证书生成参数</p>
     * @param param License校验类需要的参数
     */
    private LicenseParam initLicenseParam(LicenseVerifyParam param){
        Preferences preferences = Preferences.userNodeForPackage(LicenseVerifier.class);
        CipherParam cipherParam = new DefaultCipherParam(param.getStorePass());
        KeyStoreParam publicStoreParam = new DefaultKeyStoreParam(LicenseVerifier.class
                /** 公钥库存储路径 */
                ,param.getPublicKeysStorePath()
                /** 公匙别名 */
                ,param.getPublicAlias()
                /** 公钥库访问密码 */
                ,param.getStorePass()
                ,null);
        return new DefaultLicenseParam(param.getSubject(),preferences,publicStoreParam,cipherParam);
    }

}
